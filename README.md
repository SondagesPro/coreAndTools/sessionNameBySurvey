# Set different session name for each public survey

Allow to use a different session name for each surveys (public part). Can potentially fix issue [13761: CSRF error token - with IE 11](https://bugs.limesurvey.org/view.php?id=13761)

## Usage

This plugin is only tested with LimeSurvey 3.10 but must work with all LimeSurvey version after 2.50 build 160425

This plugin use session, then need an update on config, you must set autostrat to false :
````
	'components' => array(
		'db' => array(
		[…]
		'session' => array (
			'autoStart' => false,
			'sessionName' => "LimeSurvey",
		),
		[…]
	),
````

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/sendmailcron/>
- Copyright © 2018 Denis Chenu <http://sondages.pro>
