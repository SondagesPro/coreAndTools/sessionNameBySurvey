<?php
/**
 * sessionNameBySurvey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2018 Denis Chenu <http://www.sondages.pro>
 * @license GPL v3
 * @version 0.0.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class sessionNameBySurvey extends PluginBase {

    protected $storage = 'DbStorage';
    static protected $description = 'Use a different session name by survey';
    static protected $name = 'sessionNameBySurvey';

    function init()
    {
        $this->subscribe('beforeControllerAction');
    }

    public function beforeControllerAction() {
        $controller = App()->getController()->getId();
        if($this->getEvent()->get('controller') == 'survey') {
            $sid = App()->getRequest()->getParam('surveyid',App()->getRequest()->getParam('sid'));
            $currentName = App()->getSession()->getSessionName();
            App()->getSession()->setSessionName($currentName.'_survey_'.$sid);
        }
        if(!App()->getSession()->isStarted) {
            App()->getSession()->open();
        }
    }
}
